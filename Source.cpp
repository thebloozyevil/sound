#include <iostream>
class Animal
{
public:
    virtual void makeSound() const = 0;
};

class Bird :public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Chip\n";
    }
};

class Cow :public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Moo\n";
    }
};

class Cat : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Meow\n";
    }
};

class Dog : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Woof\n";
    }
};

int main()
{
    Animal* animals[4];
    animals[0] = new Bird();
    animals[1] = new Cow();
    animals[2] = new Cat();
    animals[3] = new Dog();

    for (Animal* a : animals)
        a->makeSound();
}